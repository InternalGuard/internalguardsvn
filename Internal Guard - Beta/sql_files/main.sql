-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 12-Jul-2015 às 22:28
-- Versão do servidor: 5.6.24
-- PHP Version: 5.5.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ragnarok`
--

DROP TABLE IF EXISTS banidos;
DROP TABLE IF EXISTS cmd;
DROP TABLE IF EXISTS conf;
DROP TABLE IF EXISTS events;
DROP TABLE IF EXISTS hashs;
DROP TABLE IF EXISTS imagens;
DROP TABLE IF EXISTS jogadoresid;
DROP TABLE IF EXISTS logs;


-- --------------------------------------------------------

--
-- Estrutura da tabela `banidos`
--

CREATE TABLE IF NOT EXISTS `banidos` (
  `id` int(10) unsigned NOT NULL,
  `UniqueID` varchar(255) NOT NULL,
  `Nick` varchar(255) DEFAULT NULL,
  `IP` varchar(100) NOT NULL,
  `Data` date NOT NULL,
  `Hora` time NOT NULL,
  `Motivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cmd`
--

CREATE TABLE IF NOT EXISTS `cmd` (
  `id` int(10) unsigned NOT NULL,
  `UniqueID` varchar(255) NOT NULL,
  `cmd` varchar(255) NOT NULL,
  `Data` date NOT NULL,
  `Hora` time NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `conf`
--

CREATE TABLE IF NOT EXISTS `conf` (
  `id` int(10) unsigned NOT NULL,
  `msg_title` varchar(255) NOT NULL,
  `msg_body` varchar(255) NOT NULL,
  `autoban` varchar(255) NOT NULL,
  `dc` varchar(255) NOT NULL,
  `cheats_allow` varchar(40000) NOT NULL,
  `ig_version` varchar(255) NOT NULL,
  `db_version` varchar(255) NOT NULL,
  `ragnarokwindowname` varchar(255) DEFAULT NULL,
  `port` varchar(255) DEFAULT NULL,
  `igkey` varchar(255) DEFAULT NULL,
  `BanOnDetect` longtext,
  `Protection` varchar(255) DEFAULT NULL,
  `Login Packet Delay` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `conf`
--

INSERT INTO `conf` (`id`, `msg_title`, `msg_body`, `autoban`, `dc`, `cheats_allow`, `ig_version`, `db_version`, `ragnarokwindowname`, `port`, `igkey`, `BanOnDetect`, `Protection`, `Login Packet Delay`) VALUES
(1, 'Internal Guard 4.5', 'My message!', 'no', '10', '#', '4.5', '25.0.0.1', 'My Ragnarok client title', '6900', '69e87709f68374fe0', 'NULL', '2', '300');

-- --------------------------------------------------------

--
-- Estrutura da tabela `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `id` int(10) unsigned NOT NULL,
  `message` varchar(255) NOT NULL,
  `Data` date NOT NULL,
  `Hora` time NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `events`
--

INSERT INTO `events` (`id`, `message`, `Data`, `Hora`) VALUES
(1, 'Welcome to Internal Guard panel .V1.3 for Windows', '2015-07-120', '15:22:17');

-- --------------------------------------------------------

--
-- Estrutura da tabela `hashs`
--

CREATE TABLE IF NOT EXISTS `hashs` (
  `id` int(10) unsigned NOT NULL,
  `file01` varchar(255) NOT NULL,
  `hash01` varchar(255) NOT NULL,
  `file02` varchar(255) NOT NULL,
  `hash02` varchar(255) NOT NULL,
  `file03` varchar(255) NOT NULL,
  `hash03` varchar(255) NOT NULL,
  `file04` varchar(255) NOT NULL,
  `hash04` varchar(255) NOT NULL,
  `file05` varchar(255) NOT NULL,
  `hash05` varchar(255) NOT NULL,
  `file06` varchar(255) NOT NULL,
  `hash06` varchar(255) NOT NULL,
  `file07` varchar(255) NOT NULL,
  `hash07` varchar(255) NOT NULL,
  `file08` varchar(255) NOT NULL,
  `hash08` varchar(255) NOT NULL,
  `file09` varchar(255) NOT NULL,
  `hash09` varchar(255) NOT NULL,
  `file10` varchar(255) NOT NULL,
  `hash10` varchar(255) NOT NULL,
  `file11` varchar(255) NOT NULL,
  `hash11` varchar(255) NOT NULL,
  `file12` varchar(255) NOT NULL,
  `hash12` varchar(255) NOT NULL,
  `file13` varchar(255) NOT NULL,
  `hash13` varchar(255) NOT NULL,
  `file14` varchar(255) NOT NULL,
  `hash14` varchar(255) NOT NULL,
  `file15` varchar(255) NOT NULL,
  `hash15` varchar(255) NOT NULL,
  `file16` varchar(255) NOT NULL,
  `hash16` varchar(255) NOT NULL,
  `file17` varchar(255) NOT NULL,
  `hash17` varchar(255) NOT NULL,
  `file18` varchar(255) NOT NULL,
  `hash18` varchar(255) NOT NULL,
  `file19` varchar(255) NOT NULL,
  `hash19` varchar(255) NOT NULL,
  `file20` varchar(255) NOT NULL,
  `hash20` varchar(255) NOT NULL,
  `file21` varchar(255) NOT NULL,
  `hash21` varchar(255) NOT NULL,
  `file22` varchar(255) NOT NULL,
  `hash22` varchar(255) NOT NULL,
  `file23` varchar(255) NOT NULL,
  `hash23` varchar(255) NOT NULL,
  `file24` varchar(255) NOT NULL,
  `hash24` varchar(255) NOT NULL,
  `file25` varchar(255) NOT NULL,
  `hash25` varchar(255) NOT NULL,
  `file26` varchar(255) NOT NULL,
  `hash26` varchar(255) NOT NULL,
  `file27` varchar(255) NOT NULL,
  `hash27` varchar(255) NOT NULL,
  `file28` varchar(255) NOT NULL,
  `hash28` varchar(255) NOT NULL,
  `file29` varchar(255) NOT NULL,
  `hash29` varchar(255) NOT NULL,
  `file30` varchar(255) NOT NULL,
  `hash30` varchar(255) NOT NULL,
  `file31` varchar(255) NOT NULL,
  `hash31` varchar(255) NOT NULL,
  `file32` varchar(255) NOT NULL,
  `hash32` varchar(255) NOT NULL,
  `file33` varchar(255) NOT NULL,
  `hash33` varchar(255) NOT NULL,
  `file34` varchar(255) NOT NULL,
  `hash34` varchar(255) NOT NULL,
  `file35` varchar(255) NOT NULL,
  `hash35` varchar(255) NOT NULL,
  `file36` varchar(255) NOT NULL,
  `hash36` varchar(255) NOT NULL,
  `file37` varchar(255) NOT NULL,
  `hash37` varchar(255) NOT NULL,
  `file38` varchar(255) NOT NULL,
  `hash38` varchar(255) NOT NULL,
  `file39` varchar(255) NOT NULL,
  `hash39` varchar(255) NOT NULL,
  `file40` varchar(255) NOT NULL,
  `hash40` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `hashs`
--

INSERT INTO `hashs` (`id`, `file01`, `hash01`, `file02`, `hash02`, `file03`, `hash03`, `file04`, `hash04`, `file05`, `hash05`, `file06`, `hash06`, `file07`, `hash07`, `file08`, `hash08`, `file09`, `hash09`, `file10`, `hash10`, `file11`, `hash11`, `file12`, `hash12`, `file13`, `hash13`, `file14`, `hash14`, `file15`, `hash15`, `file16`, `hash16`, `file17`, `hash17`, `file18`, `hash18`, `file19`, `hash19`, `file20`, `hash20`, `file21`, `hash21`, `file22`, `hash22`, `file23`, `hash23`, `file24`, `hash24`, `file25`, `hash25`, `file26`, `hash26`, `file27`, `hash27`, `file28`, `hash28`, `file29`, `hash29`, `file30`, `hash30`, `file31`, `hash31`, `file32`, `hash32`, `file33`, `hash33`, `file34`, `hash34`, `file35`, `hash35`, `file36`, `hash36`, `file37`, `hash37`, `file38`, `hash38`, `file39`, `hash39`, `file40`, `hash40`) VALUES
(1, 'ragexe.exe', '3225749292', 'InternalGuard32.exe', '1712124393', 'InternalGuard64.exe', '1310164066', 'Internal.grf', '3838052520', 'InternalGuard\\conf.ini', '511162972', 'InternalGuard\\Gcore32.dll', '3049229463', 'InternalGuard\\Gcore64.dll', '3920573743', 'InternalGuard\\IG.dll', '3886488501', 'InternalGuard\\ig.ini', '634393303', 'InternalGuard\\i32.sys', '547369278', 'InternalGuard\\i64.sys', '2225866781', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `imagens`
--

CREATE TABLE IF NOT EXISTS `imagens` (
  `id` int(10) unsigned NOT NULL,
  `UniqueID` varchar(255) NOT NULL,
  `Nick` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `Data` date NOT NULL,
  `Hora` time NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `jogadoresid`
--

CREATE TABLE IF NOT EXISTS `jogadoresid` (
  `id` int(10) unsigned NOT NULL,
  `UniqueID` varchar(255) NOT NULL,
  `Accountid` varchar(255) NOT NULL,
  `Charid` varchar(255) NOT NULL,
  `Nick` varchar(255) NOT NULL,
  `Map` varchar(255) NOT NULL,
  `IP` varchar(100) NOT NULL,
  `PDate` date NOT NULL,
  `PTime` time NOT NULL,
  `POnline` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Estrutura da tabela `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(10) unsigned NOT NULL,
  `UniqueID` varchar(255) NOT NULL,
  `Accountid` varchar(255) NOT NULL,
  `Charid` varchar(255) NOT NULL,
  `Nick` varchar(255) NOT NULL,
  `Map` varchar(255) NOT NULL,
  `IP` varchar(100) NOT NULL,
  `PDate` date NOT NULL,
  `PTime` time NOT NULL,
  `POnline` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banidos`
--
ALTER TABLE `banidos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cmd`
--
ALTER TABLE `cmd`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imagens`
--
ALTER TABLE `imagens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jogadoresid`
--
ALTER TABLE `jogadoresid`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`Nick`,`Accountid`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banidos`
--
ALTER TABLE `banidos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cmd`
--
ALTER TABLE `cmd`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `imagens`
--
ALTER TABLE `imagens`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `jogadoresid`
--
ALTER TABLE `jogadoresid`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
